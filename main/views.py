# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect, reverse


def index(request):
    return render(request, 'main/index.html')


def submit(request):
    return redirect(reverse('main:index'))
